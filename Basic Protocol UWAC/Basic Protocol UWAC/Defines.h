/*
 * Defines.h
 *
 * Created: 29-1-2016 15:42:49
 *  Author: Kevin Slagmolen
 */ 


#ifndef DEFINES_H_
#define DEFINES_H_

#define DEBUG					// Comment this line to DISable debug mode
#define BAUD_RATE	1			// The baud rate of the communication
#define BITS		4			// The amount of bits used to send the data
#define SYMBOLS		2 ^ BITS	// The amount of usable symbols to send

#endif /* DEFINES_H_ */