/*
 * UWAC_protocol.h
 *
 * Created: 29-1-2016 15:40:47
 *  Author: Kevin Slagmolen
 */ 


#ifndef UWAC_PROTOCOL_H_
#define UWAC_PROTOCOL_H_

#include "Defines.h"

void init_protocol();
void init_protocol(uint8_t inputPort, uint8_t input[BITS], uint8_t outputPort, uint8_t output[BITS]);

#endif /* UWAC_PROTOCOL_H_ */