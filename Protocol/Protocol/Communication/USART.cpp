/*
 * USART.cpp
 *
 * Created: 7-3-2016 13:22:12
 *  Author: Kevin Slagmolen
 */ 

#include "USART.h"

void USART::init()
{
	// Set baud rate
	UBRR0H = (uint8_t)(UBRR_VALUE >> 8);
	UBRR0L = (uint8_t)UBRR_VALUE;

	// Set frame format to 8 data bits, no parity, 1 stop bit
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);
	// Enable transmission
	UCSR0B |= (1 << TXEN0);
}

int USART::SendByte(char u8Data, FILE *stream)
{
	if (u8Data == '\n')
	{
		SendByte('\r', stream);
	}

	// Wait while previous byte is completed
	while (!(UCSR0A & (1 << UDR0)));

	// Transmit data
	UDR0 = u8Data;
	return 0;
}