/*
 * USART.h
 *
 * Created: 7-3-2016 13:22:02
 *  Author: Kevin Slagmolen
 */ 

#ifndef USART_H_
#define USART_H_

#include "../Util/defines.h"

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

class USART
{
public:
	USART();
	~USART();

	void init();

	static int SendByte(char u8Data, FILE *stream);
};

#endif /* USART_H_ */