/*
 * Protocol.cpp
 *
 * Created: 7-3-2016 13:18:08
 * Author : Kevin Slagmolen
 */ 

#include <avr/io.h>

#include "Util/defines.h"
#include "Communication/USART.h"

USART* serial = NULL;

FILE usart0_str = FDEV_SETUP_STREAM(serial->SendByte, NULL, _FDEV_SETUP_WRITE);

int main(void)
{
	serial->init();
	stdout =& usart0_str;

    while (1) 
    {
		printf("test\n");
    }
}

