/*
 * Detect_With_Timer.c
 *
 * Created: 6-10-2015 13:18:50
 *  Author: Kevin Slagmolen
 */ 


#include <avr/io.h>

#define F_CPU 16000000UL
#include <util/delay.h>
#include <avr/interrupt.h>

#define MAINPERIOD 100 // Measurement frame in microseconds
#define F_INPUT_PIN DDD2

#define LED DDB5

// Store value of microseconds since uC started
volatile unsigned long micros_value;

long previousMicros;
unsigned long duration;
long pulseCount;
int i, j;

int main(void)
{
	previousMicros = 0;
	duration = 0;
	pulseCount = 0;
	i = 0;
	j = 0;
	
	TCCR0A |= (1 << WGM01);
	
	OCR0A = 0xF;
	
	TIMSK0 |= (1 << OCIE0A);
	
	PCMSK0 |= (1 << PCINT0);
	PCICR |= (1 << PCIE0);
	
	sei();
	
	DDRD |= ~(1 << F_INPUT_PIN);
	DDRB |= (1 << LED);
	
    while(1)
    {
		cli();
		unsigned long currentMicros = micros_value;
		sei();
		
		if (currentMicros - previousMicros >= MAINPERIOD)
		{
			previousMicros = currentMicros;
			
			float Freq = 0.5e6/float(duration);
			Freq *= pulseCount;
			
			duration = 0;
			pulseCount = 0;
			
			if (Freq < 43000)
			{
				i++;
				j = 0;
				if (i == 5)
				{
					PORTB &= ~(1 << LED);
					i = 0;
				}
			}
			else
			{
				j++;
				i = 0;
				if (j == 4)
				{
					PORTB |= (1 << LED);
					j = 0;
				}
			}
		}
		duration += pulseIn(F_INPUT_PIN, HIGH, MAINPERIOD * 900);
		pulseCount++;
    }
}

ISR (TIMER0_COMPA_vect)
{
	micros_value++;
}

ISR (PCINT0_vect)
{
	if (PINB & (1 <<))
}