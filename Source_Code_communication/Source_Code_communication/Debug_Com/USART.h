/*
 * USART.h
 *
 * Created: 11-9-2015 19:21:02
 *  Author: Kevin Slagmolen
 */ 


#ifndef USART_H_
#define USART_H_

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef F_CPU
	#define F_CPU 16000000UL
#endif

#define USART_BAUDRATE 500
#define UBRR_VALUE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

void USARTInit(void);
int USARTSendByte(char u8Data, FILE *stream);

#endif /* USART_H_ */