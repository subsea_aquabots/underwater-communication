/*
 * USART.c
 *
 * Created: 11-9-2015 19:20:54
 *  Author: Kevin Slagmolen
 */ 

#include "USART.h"

/**
 *	USART0Init
 *
 *	Initialize the USART communication with the USART_BAUDRATE
 */
void USARTInit(void)
{
	// Set the baud rate
	UBRR0H = (uint8_t)(UBRR_VALUE >> 8);
	UBRR0L = (uint8_t)UBRR_VALUE;
	// Set frame format to 8 data bits, no parity, 1 stop bit
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);
	// Enable transmission and reception
	UCSR0B |= (1 << RXEN0) | (1 << TXEN0);
}

/**
 *	USART0SendByte
 *
 *
 *	@param (u8Data) the data to be send
 *
 *	@return returns a zero if completed
 */
int USARTSendByte(char u8Data, FILE *stream)
{
	if (u8Data == '\n')
	{
		USARTSendByte('\r', stream);
	}
	
	// Wait while previous byte is completed
	while (!(UCSR0A & (1 << UDRE0)));
	// Transmit data
	UDR0 = u8Data;
	return 0;
}