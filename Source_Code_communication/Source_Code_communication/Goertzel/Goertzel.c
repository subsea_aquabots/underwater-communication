/*
 * Goertzel.c
 *
 * Created: 11-9-2015 14:16:29
 *  Author: Kevin Slagmolen
 */ 

#include "Goertzel.h"

/**
 * GoertzelInit
 *
 * Initialize the basic stuff for the Goertzel Algorithm (https://en.wikipedia.org/wiki/Goertzel_algorithm)
 */
void GoertzelInit()
{
	float omega = (2.0 * PI * TARGET_FREQUENCY) / SAMPLING_FREQUENCY;
	coeff = 2.0 * cos(omega);
	
	GoertzelReset();
}

void GoertzelReset()
{
	Q2 = 0;
	Q1 = 0;
}

void GoertzelSample(int ADCreg)
{
	int i;
	for (i = 0; i < N; i++)
	{
		testData[i] = ADCRead(ADCreg);
	}
}

float GoertzelDetect()
{
	float magnitude;
	
	int i;
	// Process the samples
	for (i = 0; i < N; i++)
	{
		GoertzelProcessSample(testData[i]);
	}
	
	// Calculate the magnitude of the signal
	magnitude = sqrt(Q1 * Q1 + Q2 * Q2 - coeff * Q1 * Q2);
	
	GoertzelReset();
	return magnitude;
}

void GoertzelProcessSample(int sample)
{
	float Q0 = coeff * Q1 - Q2 + (float) (sample - ADCCENTER);
	Q2 = Q1;
	Q1 = Q0;
}