/*
 * Goertzel.h
 *
 * Created: 11-9-2015 14:16:37
 *  Author: Kevin Slagmolen
 */ 


#ifndef GOERTZEL_H_
#define GOERTZEL_H_

#include <math.h>

#include "../ADC/ADC.h"

#define PI 3.141592653589793

#define ADCCENTER 512

#define TARGET_FREQUENCY 40000
#define N 50
#define THRESHOLD 650
#define SAMPLING_FREQUENCY 220000

void GoertzelInit();
void GoertzelReset();
void GoertzelSample(int ADCreg);
float GoertzelDetect();
void GoertzelProcessSample(int sample);

float coeff, Q1, Q2;
int testData[N];

#endif /* GOERTZEL_H_ */