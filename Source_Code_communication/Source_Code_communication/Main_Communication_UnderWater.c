/*
 * Main_Communication_UnderWater.c
 *
 * Created: 11-9-2015 14:04:47
 *  Author: Kevin Slagmolen
 */ 

#include <avr/io.h>
#include <math.h>

#define F_CPU 16000000UL
#include <util/delay.h>

#define LED DDB5

#include "Goertzel/Goertzel.h"
#include "ADC/ADC.h"
//#include "Debug_Com/USART.h"
//#include <stdio.h>

//FILE usart_str = FDEV_SETUP_STREAM(USARTSendByte, NULL, _FDEV_SETUP_WRITE);

int main(void)
{
	// Simple test programm
	DDRB |= (1 << LED); // Set the LED as an output
	PORTB &= ~(1 << LED); // Turn the LED off
	
	GoertzelInit(); // Initializes Goertzel
	
	ADCInit(); // Initializes ADC
	
	//USARTInit(); // Initializes USART communication
	
	//stdout =& usart_str;
	
	while (1)
	{
		GoertzelSample(0); // Get the data from the signal on ADC register 0
		
		float magnitude = GoertzelDetect(); // Get the magnitude of the signal
		
		// Check if the magnitude is higher than the threshold
		if (magnitude > THRESHOLD)
		{
			PORTB |= (1 << LED); // Turn the LED on
		}
		else
		{
			PORTB &= ~(1 << LED); // Turn the LED off
		}
		
		//printf("%u\n", (uint16_t)magnitude); // Print the magnitude to the serial monitor
	}
}