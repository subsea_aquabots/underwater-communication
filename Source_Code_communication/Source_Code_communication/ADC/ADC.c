/*
 * ADC.c
 *
 * Created: 11-9-2015 19:39:28
 *  Author: Kevin Slagmolen
 */ 

#include "ADC.h"

void ADCInit(void)
{
	// Select Vref = AVcc
	ADMUX |= (1 << REFS0);
	// Set prescaler to 128 and enable ADC
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0) | (1 << ADEN);
}

uint16_t ADCRead(uint8_t ADCchannel)
{
	// Select ADC channel with safety mask
	ADMUX = (ADMUX & 0xF0) | (ADCchannel & 0x0F);
	// Single conversion mode
	ADCSRA |= (1 << ADSC);
	// Wait until ADC conversion is complete
	while (ADCSRA & (1 << ADSC));
	return ADC;
}