/*
 * ADC.h
 *
 * Created: 11-9-2015 19:39:18
 *  Author: Kevin Slagmolen
 */ 


#ifndef ADC_H_
#define ADC_H_

#include <avr/io.h>

void ADCInit(void);
uint16_t ADCRead(uint8_t ADCchannel);

#endif /* ADC_H_ */