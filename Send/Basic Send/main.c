/*
 * Basic send.c
 *
 * Created: 30-1-2016 16:36:16
 * Author : Kevin Slagmolen
 */ 

#include <avr/io.h>
#include <math.h>

#define F_CPU 16000000UL

#include "defines.h"

#include "data_conversion.h"

#include "i2c.h"

// SERIAL COMMUNICATION STUFF
#include <stdio.h>
#include <stdlib.h>
#define USART_BAUDRATE 921600
#define UBRR_VALUE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)
int USART0SendByte(char u8Data, FILE *stream);
FILE usart0_str = FDEV_SETUP_STREAM(USART0SendByte, NULL, _FDEV_SETUP_WRITE);
void USART0Init(void);
// END

#include <util/delay.h>

#define DCO_PROGRAM_FREQ_1 DDB0 // Enable pin for DCO1
#define DCO_PROGRAM_FREQ_2 DDB1 // Enable pin for DCO2

#define STATUS DDB5 // Status LED

//#define DEBUG // Uncomment this line to enable DBUG prints to the serial monitor

unsigned char i2cAddress;
void setFreq(uint32_t freq);
void sendData(uint8_t dataToSend[800]);

int main(void)
{
	// init IO
	DDRB |= (1 << OUTPUT_PIN); // Set the output pin

	DDRB |= (1 << DCO_PROGRAM_FREQ_1) | (1 << DCO_PROGRAM_FREQ_2) | (1 << STATUS);

	PORTB |= (1 << STATUS); // Turn on status LED
	//_delay_ms(2000); // Wait for 2 seconds
	PORTB &= ~(1<< STATUS); // Turn off status LED

	USART0Init(); // Initialize USART communication
	stdout =& usart0_str;

	i2c_init();

	PORTB |= (1 << DCO_PROGRAM_FREQ_1);
	PORTB &= ~(1 << DCO_PROGRAM_FREQ_2);
	i2cAddress = 0x17;
	setFreq(45000);

	PORTB |= (1 << DCO_PROGRAM_FREQ_2);
	PORTB &= ~(1 << DCO_PROGRAM_FREQ_1);
	i2cAddress = 0x17;
	setFreq(55000);

	PORTB ^= (1 << STATUS);
	_delay_ms(200);
	PORTB ^= (1 << STATUS);

    char input[100] = "Now, this is a story all about how";
    printf("\n\n\n\nSTART CODE WITH THIS INPUT: %s\nBAUDRATE: %i\n\n", input, OUTPUT_BAUDRATE); // Print the input string
    
    uint8_t* dataToSend = fillDataArray(input); // Convert data to base2

    unsigned int time;

    time = (1000 / OUTPUT_BAUDRATE) * dataLength;

    printf("Baudtime: %i\n", 1000/OUTPUT_BAUDRATE);
    printf("Time needed to send: %i ms\n", time);


    printf("datalength: %i\n", dataLength);

    PORTB &= ~(1 << OUTPUT_PIN);
	
	//_delay_ms(2000);
	//sendData(dataToSend);

    while (1)
    {
		//sendData(dataToSend);
		//_delay_ms(2000);

		PORTB ^= (1 << OUTPUT_PIN);
		_delay_us(DELAY_US);
		PORTB ^= (1 << OUTPUT_PIN);
		_delay_us(DELAY_US);
    }
}

void sendData(uint8_t dataToSend[800])
{
	int i;

	for (i = 0; i < dataLength; i++)
	{
		if ((dataToSend[i] && 0b1) == 0)
		{
			PORTB &= ~(1 << OUTPUT_PIN);
		}
		else
		{
			PORTB |= (1 << OUTPUT_PIN);
		}
		_delay_us(DELAY_US);
	}

	//printf("Next one.\n");

}

void setFreq(uint32_t freq)
{
	/*
	Calculate frequency
	See datasheet for more information: http://cds.linear.com/docs/en/datasheet/69034fe.pdf (page 8)
	*/
	#ifdef DEBUG
	printf("Frequency: %" PRIu32 "\n", freq);
	#endif

	uint32_t OCT = (uint32_t)floor(3.322 * log10(freq / 1039));
	uint32_t DAC = round(2048 - (2078 * pow(2, 10 + OCT) / freq));

	#ifdef DEBUG
	printf("OCT %" PRIu32 "\nDAC: %" PRIu32 "\n", OCT, DAC);
	#endif

	uint16_t setBits = OCT << 12 | DAC << 2 | 1 << 1; // Write all the bits in one variable

	// Split the bits in a high and a low part
	char meleon = (setBits >> 8) & 0xff; // High part
	char mander = setBits & 0xff; // Low part

	#ifdef DEBUG // Calculate the exact frequency of the DCO
	uint32_t power = (uint32_t)round(pow(2, OCT));
	double div = (double)DAC / 1024.0;
	double second = 2078 / (2 - div);

	uint32_t exactFreq = power * second;

	printf("Exact frequency: %" PRIu32 "\n", exactFreq);
	#endif

	uint8_t bits[2] = {meleon, mander}; // Set the high and low part in an array

	i2c_transmit(i2cAddress << 1, bits, 2);
}

// SERIAL COMMUNICATION CODE START
void USART0Init(void)
{
	// Set baud rate
	UBRR0H = (uint8_t)(UBRR_VALUE >> 8);
	UBRR0L = (uint8_t)UBRR_VALUE;
	// Set frame format to 8 data bits, no parity, 1 stop bit
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);
	// Enable transmission
	UCSR0B |= (1 << TXEN0);
}

int USART0SendByte(char u8Data, FILE *stream)
{
	if (u8Data == '\n')
	{
		USART0SendByte('\r', stream);
	}

	// Wait while previous byte is completed
	while (!(UCSR0A & (1 << UDRE0)));

	// Transmit data
	UDR0 = u8Data;
	return 0;
}
// SERIAL COMMUNICATION CODE END