/*
 * Basic receive.c
 *
 * Created: 6-4-2016 12:34:22
 * Author : Kevin Slagmeme
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#define F_CPU 16000000UL

#include "defines.h"
#include "data_conversion.h"


// SERIAL COMMUNICATION STUFF
#include <stdio.h>
#include <stdlib.h>
#define USART_BAUDRATE 921600
#define UBRR_VALUE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)
int USART0SendByte(char u8Data, FILE *stream);
FILE usart0_str = FDEV_SETUP_STREAM(USART0SendByte, NULL, _FDEV_SETUP_WRITE);
void USART0Init(void);
// END

#include <util/delay.h>

//#define DEBUG // Uncomment this line to enable DBUG prints to the serial monitor

uint8_t receivedData[800];

volatile uint8_t receiving;

ISR(PCINT0_vect)
{
	receiving = 1;
	cli(); // Disable interrupts
}

int main(void)
{
	int i;

	for (i = 0; i < 800; i++)
	{
		receivedData[i] = 0;
 	}

	receiving = 0;

	USART0Init(); // Initialize USART communication
	stdout =& usart0_str;

	printf("Start baudrate: %i\n", OUTPUT_BAUDRATE);

    DDRB &= ~(1 << INPUT_PIN); // Set INPUT_PIN as input
	//PORTB |= (1 << INPUT_PIN); // Enable pull-up resistor

	PCICR |= (1 << PCIE0);
	PCMSK0 |= (1 << PCINT0);

	sei(); // Turn on interrupts

	int counter = 0;

    while (1) 
    {
		/*while (receiving == 1)
		{
			//printf("%i, receiving...\n", counter);

			_delay_us(DELAY_US / 2);
			
			receivedData[counter] = PINB & (1 << INPUT_PIN);

			_delay_us(DELAY_US / 2);

			uint8_t memes1 = PINB & (1 << INPUT_PIN);

			_delay_ms((1000 / OUTPUT_BAUDRATE) / 4);

			//receivedData[counter] = (memes0 + memes1) / 2;
			//printf("%i: ", counter);

			//printf("%i\n", receivedData[counter]);

			counter++;
			
			if (counter > 800)
			{
				printf("\n\nStop receiving\n\n\n");
				
				int i;

				for (i = 0; i < counter; i++)
				{
					printf("%i", receivedData[i]);

					if ((i + 1) % 8 == 0) printf(" ");
				}

				char* received = convertToData(receivedData, counter); // Convert base2 to data

				if (received[0] != 0b11111111)
				{
					printf("Error detected...\n");
				}
				else
				{
					printf("\nReceived data: %s\n\n", received); // Print the received data
				}

				counter = 0;
				receiving = 0;
			}
		}*/
    }
}

// SERIAL COMMUNICATION CODE START
void USART0Init(void)
{
	// Set baud rate
	UBRR0H = (uint8_t)(UBRR_VALUE >> 8);
	UBRR0L = (uint8_t)UBRR_VALUE;
	// Set frame format to 8 data bits, no parity, 1 stop bit
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);
	// Enable transmission
	UCSR0B |= (1 << TXEN0);
}

int USART0SendByte(char u8Data, FILE *stream)
{
	if (u8Data == '\n')
	{
		USART0SendByte('\r', stream);
	}

	// Wait while previous byte is completed
	while (!(UCSR0A & (1 << UDRE0)));

	// Transmit data
	UDR0 = u8Data;
	return 0;
}
// SERIAL COMMUNICATION CODE END