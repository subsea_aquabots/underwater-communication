/*
** FILE: main.c
** Authors: Kevin Slagmolen
** Date: 19-03-2016
** Description: A program that can detect and fix some errors
*/

#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>

#define POLY 0x167

// Variables
uint8_t dataToSend[400][2];
uint8_t receivedData[400][2];

int dataLength;

char convertedData[100];

int errorChange;

//Functions
void fillDataArray(char data[100]);
void convertToData(uint8_t bits[400][2]);
void errorSimulation(uint8_t bits[400][2]);
void addErrorDetectBits(uint8_t bits[400][2]);
int CRC(int data[100], int length, uint16_t poly, bool received);

int main()
{
    errorChange = 10;

    char input[100];

    printf("Enter a string: ");
    scanf("%99[^\n]", &input);

    int i;

    int ASCII[100];

    for (i = 0; i < strlen(input); i++)
    {
        ASCII[i] = (uint8_t)input[i];
    }

    printf("\nCalculate CRC checksum:\n");
    int checksum = CRC(ASCII, strlen(input), POLY, false);

    printf("\n\nEnter the error rate (0-100): ");
    scanf("%i", &errorChange);

    fillDataArray(input); // Fill in the array

    dataToSend[dataLength][0] = (checksum & 0b10000000) >> 7;
    dataToSend[dataLength][1] = (checksum & 0b01000000) >> 6;
    dataToSend[dataLength + 1][0] = (checksum & 0b00100000) >> 5;
    dataToSend[dataLength + 1][1] = (checksum & 0b00010000) >> 4;
    dataToSend[dataLength + 2][0] = (checksum & 0b00001000) >> 3;
    dataToSend[dataLength + 2][1] = (checksum & 0b00000100) >> 2;
    dataToSend[dataLength + 3][0] = (checksum & 0b00000010) >> 1;
    dataToSend[dataLength + 3][1] = (checksum & 0b00000001) >> 0;

    dataLength += 4;

    // Write to array to the screen
    printf("Good bits\n");
    for (i = 0; i < dataLength; i++)
    {
        printf("%i%i", dataToSend[i][0], dataToSend[i][1]);
        if ((i + 1) % 4 == 0) printf(" ");
    }

    srand(time(NULL));

    printf("\n\nBad bits\n");

    errorSimulation(dataToSend);

    for (i = 0; i < dataLength; i++)
    {
        printf("%i%i", receivedData[i][0], receivedData[i][1]);
        if ((i + 1) % 4 == 0) printf(" ");
    }
    printf("\n");
    printf("\nChanged bits:\n");
    for (i = 0; i < dataLength; i++)
    {
        if (dataToSend[i][0] == receivedData[i][0]) printf("0");
        else printf("1");
        if (dataToSend[i][1] == receivedData[i][1]) printf("0");
        else printf("1");

        if ((i + 1) % 4 == 0) printf(" ");
    }


    convertToData(receivedData);

    printf("\n\nReceived data:\n%s\n", convertedData);

    for (i = 0; i < 100; i++)
    {
        ASCII[i] = 0;
    }

    int j = 0;

    char tmpChar = 0b00000000;

    char convertedForCRC[100] = "";

    for (i = 0; i < dataLength; i++)
    {
        tmpChar = tmpChar << 2 | (receivedData[i][0] << 1 | receivedData[i][1]);
        if ((i + 1) % 4 == 0)
        {
            convertedForCRC[j] = tmpChar;
            j++;
            tmpChar = 0b00000000;
        }
    }


    for (i = 0; i < strlen(convertedForCRC); i++)
    {
        ASCII[i] = (uint8_t)convertedForCRC[i];
    }

    printf("\Check for errors with CRC:\n");
    int check = CRC(ASCII, strlen(convertedForCRC), POLY, true);

    if (check == 0) printf("No errors detected");
    else printf("Error detected");

    return 0;
}

int CRC(int data[100], int length, uint16_t poly, bool received)
{
    int i;

    int allData = 0;

    if (received)
    {
        for (i = 0; i < length; i++)
        {
            allData = allData << 8 | data[i];
        }

        printf("\nallData: %i\n", allData);
    }
    else
    {
        for (i = 0; i < length; i++)
        {
            allData = allData << 8 | (int)data[i];
        }

        printf("\nallData: %i\n", allData);

        allData = allData << 8;
    }

    printf("%i\n", allData);

    uint64_t calcPoly = poly;

    while (1)
    {
        bool shiftBack = false;

        if (allData < 0xFF) break;
        while(calcPoly < allData)
        {
            calcPoly = calcPoly << 1;
            shiftBack = true;
        }
        if(shiftBack) calcPoly = calcPoly >> 1;

        printf("%i^%i=", allData, calcPoly);

        allData = allData ^ calcPoly;

        printf("%i\n", allData);

        calcPoly = poly;
    }

    return allData;
}

void errorSimulation(uint8_t bits[400][2])
{
    int i;

    for (i = 0; i < dataLength; i++)
    {
        receivedData[i][0] = bits[i][0];
        receivedData[i][1] = bits[i][1];
        if ((rand() % 100) < errorChange)
        {
            //Flip this bit
            uint8_t random = rand() % 2;
            receivedData[i][random] = !receivedData[i][random];
            printf("#%i ", i);
        }
    }
    printf("\n");
}

void convertToData(uint8_t bits[400][2])
{
    int i, j;

    j = 0;

    char tmpChar = 0b00000000;

    for (i = 0; i < dataLength; i++)
    {
        tmpChar = tmpChar << 2 | (bits[i][0] << 1 | bits[i][1]);
        if ((i + 1) % 4 == 0)
        {
            convertedData[j] = tmpChar;
            j++;
            tmpChar = 0b00000000;
        }
    }

    // Delete the last one because it is for error checking
    convertedData[j - 1] = 0b00000000;
}

void fillDataArray(char data[100])
{
    int i;

    for (i = 0; i < strlen(data); i++)
    {
        uint8_t j;
        uint8_t ascii = (uint8_t)data[i];

        for (j = 0; j < 4; j++)
        {
            uint8_t tmp = (ascii & 0b11000000) >> 6;
            ascii = ascii << 2;

            dataToSend[i*4+j][0] = (tmp & 0b10) >> 1;
            dataToSend[i*4+j][1] = (tmp & 0b01);

            dataLength++;
        }
    }
}
