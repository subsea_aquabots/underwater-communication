/*
** FILE: main.c
** Authors: Kevin Slagmolen
** Date: 16-03-2016
** Description: A program that converts a string to a 2symbol array without much debugging stuff
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>

// Variables
bool dataToSend[400][2];
int dataLength;

//Functions
void fillDataArray(char data[100]);

int main()
{
    char input[100];

    printf("Enter a string: ");
    scanf("%99[^\n]", &input);

    fillDataArray(input); // Fill in the array

    int i;

    // Write to array to the screen
    for (i = 0; i < dataLength; i++)
    {
        printf("%i%i\n", dataToSend[i][0], dataToSend[i][1]);
    }

    printf("Donzo");

    return 0;
}

void fillDataArray(char data[100])
{
    int i;

    for (i = 0; i < strlen(data); i++)
    {
        uint8_t j;
        uint8_t ascii = (uint8_t)data[i];

        for (j = 0; j < 4; j++)
        {
            uint8_t tmp = (ascii & 0b11000000) >> 6;
            ascii = ascii << 2;

            dataToSend[i*4+j][0] = tmp & 0b10;
            dataToSend[i*4+j][1] = tmp & 0b01;

            dataLength++;
        }
    }
}
