/*
** FILE: main.c
** Authors: Kevin Slagmolen
** Date: 15-03-2016
** Description: A program that converts a string to a 2symbol array
** Everything works except the part at lines 60 and 61
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool dataToSend[400][2];
int dataLength;

int main()
{
    int i, j;

    dataLength = 0;

    // Reset all the array to 0 (to clear some memory)
    for (i = 0; i < 64; i++)
    {
        for (j = 0; j < 2; j++)
        {
            dataToSend[i][j] = NULL;
        }
    }

    char input[100];
    // Set all the characters in the string to nothing so we can detect that later on
    for (i = 0; i < 100; i++)
    {
        input[i] = NULL;
    }

    // Ask for some stuff to convert
    printf("Enter a string to convert to a 2 symbol array.\n");
    scanf("%99[^\n]", &input);
    printf("\nReceived data '%s'\n", &input); // Put it back on the screen

    int finalSend;

    // Loop through the max value of the string
    for (i = 0; i < 100; i++)
    {
        if (input[i] == NULL) break; // If it is not a valid character break out this loop

        printf("#%i: ASCII(%c), DEC(%i), ", i, input[i], (int)input[i]); // Some debug stuff

        int data = (int)input[i]; // Convert the char to decimal

        // Some debug stuff
        char baseConvert[8];
        itoa(data, baseConvert, 2);
        int baseTwo = atoi(&baseConvert);

        printf("BASE2(%i), DEBUG(", baseTwo); // More debugging

        // Loop 4 times to convert it to base4(ish)
        for (j = 0; j < 4; j++)
        {
            int tmp = (data & 0b11000000) >> 6; // Only select the 2 first bits and shift them to the last places
            data = data << 2; // Shift the data

            dataToSend[(i*4)+j][0] = tmp & 0b10;
            dataToSend[(i*4)+j][1] = tmp & 0b01;

            printf("%i", tmp); // Debugging, what else?

            if (j != 3) printf(", ");
            dataLength++;
        }
        printf(")\n"); // When does the debugging stop?
    }

    printf("Amount of data: %i\n\nARRAY WITH DATA:\n", dataLength);

    for (i = 0; i < dataLength; i++)
    {
        printf("%i%i\n", dataToSend[i][0], dataToSend[i][1]);
        //printf("#%i: F(%i), S(%i)\n", i, dataToSend[i][0], dataToSend[i][1]);
    }

    return 0;
}
