/*
** FILE: main.c
** Authors: Kevin Slagmolen
** Date: 16-03-2016
** Description: A program that converts a 4 symbol array to data
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>

// Variables
int dataToSend[400][2];
int dataLength;
char convertedData[100];

//Functions
void fillDataArray(char data[100]);
void convertToData(int bits[400][2]);

int main()
{
    char input[100];

    printf("Enter a string: ");
    scanf("%99[^\n]", &input);

    fillDataArray(input); // Fill in the array

    int i;

    // Write to array to the screen
    for (i = 0; i < dataLength; i++)
    {
        printf("%i%i\n", dataToSend[i][0], dataToSend[i][1]);
    }

    convertToData(dataToSend);

    printf("%s\n", convertedData);

    printf("Donzo");

    return 0;
}

void convertToData(int bits[400][2])
{
    int i, j;

    i=j=0;

    char tmpChar = 0b00000000;

    for (i = 0; i < dataLength; i++)
    {
        tmpChar = tmpChar << 2 | (bits[i][0] << 1 | bits[i][1]);
        if ((i + 1) % 4 == 0)
        {
            convertedData[j] = tmpChar;
            j++;
            tmpChar = 0b00000000;
        }
    }
}

void fillDataArray(char data[100])
{
    int i;

    for (i = 0; i < strlen(data); i++)
    {
        uint8_t j;
        uint8_t ascii = (uint8_t)data[i];

        for (j = 0; j < 4; j++)
        {
            uint8_t tmp = (ascii & 0b11000000) >> 6;
            ascii = ascii << 2;

            dataToSend[i*4+j][0] = (tmp & 0b10) >> 1;
            dataToSend[i*4+j][1] = (tmp & 0b01);

            dataLength++;
        }
    }
}
