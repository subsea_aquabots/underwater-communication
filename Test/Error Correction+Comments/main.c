#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "data_conversion.h"

int main()
{
    char input[100];

    // Input stuff
    printf("Enter a string: ");
    scanf("%99[^\n]", &input);

    int* dataToSend = fillDataArray(input); // Fill the data in the array

    char* received = convertToData(dataToSend, dataLength); // Convert the data back to a string

    if (received[0] == -1) printf("ERROR DETECTED.\n");

    printf("\n\nReceived data: %s\n", received); // Print the received data

    return 0;
}
