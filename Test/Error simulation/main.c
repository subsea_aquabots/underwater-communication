/*
** FILE: main.c
** Authors: Kevin Slagmolen
** Date: 16-03-2016
** Description: A program that simulates errors in the physical layer
*/

#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>

// Variables
uint8_t dataToSend[400][2];
uint8_t receivedData[400][2];

int dataLength;

char convertedData[100];

int errorChange;

//Functions
void fillDataArray(char data[100]);
void convertToData(uint8_t bits[400][2]);
void errorSimulation(uint8_t bits[400][2]);

int main()
{
    errorChange = 10;

    char input[100];

    printf("Enter a string: ");
    scanf("%99[^\n]", &input);
    fillDataArray(input); // Fill in the array
    printf("Amount of bits: %i\n", dataLength * 2);
    printf("Enter the change (0-100): ");
    scanf("%i", &errorChange);

    int i;

    // Write to array to the screen
    printf("Good bits\n");
    for (i = 0; i < dataLength; i++)
    {
        printf("%i%i", dataToSend[i][0], dataToSend[i][1]);
        if ((i + 1) % 4 == 0) printf(" ");
    }

    srand(time(NULL));

    printf("\n\nBad bits\n");

    errorSimulation(dataToSend);

    printf("\n");
    for (i = 0; i < dataLength; i++)
    {
        printf("%i%i", receivedData[i][0], receivedData[i][1]);
        if ((i + 1) % 4 == 0) printf(" ");
    }
    printf("\n");
    printf("\nChanged bits:\n");
    for (i = 0; i < dataLength; i++)
    {
        if (dataToSend[i][0] == receivedData[i][0]) printf("0");
        else printf("1");
        if (dataToSend[i][1] == receivedData[i][1]) printf("0");
        else printf("1");

        if ((i + 1) % 4 == 0) printf(" ");
    }


    convertToData(receivedData);

    printf("\n\nReceived data:\n%s\n", convertedData);

    printf("Donzo");

    return 0;
}

void errorSimulation(uint8_t bits[400][2])
{
    int i;

    for (i = 0; i < dataLength; i++)
    {
        receivedData[i][0] = bits[i][0];
        receivedData[i][1] = bits[i][1];
        if ((rand() % 100) < errorChange)
        {
            //Flip this bit
            uint8_t random = rand() % 2;
            receivedData[i][random] = !receivedData[i][random];
            printf("#%i ", i);
        }
    }
}

void convertToData(uint8_t bits[400][2])
{
    int i, j;

    j = 0;

    char tmpChar = 0b00000000;

    for (i = 0; i < dataLength; i++)
    {
        tmpChar = tmpChar << 2 | (bits[i][0] << 1 | bits[i][1]);
        if ((i + 1) % 4 == 0)
        {
            convertedData[j] = tmpChar;
            j++;
            tmpChar = 0b00000000;
        }
    }
}

void fillDataArray(char data[100])
{
    int i;

    for (i = 0; i < strlen(data); i++)
    {
        uint8_t j;
        uint8_t ascii = (uint8_t)data[i];

        for (j = 0; j < 4; j++)
        {
            uint8_t tmp = (ascii & 0b11000000) >> 6;
            ascii = ascii << 2;

            dataToSend[i*4+j][0] = (tmp & 0b10) >> 1;
            dataToSend[i*4+j][1] = (tmp & 0b01);

            dataLength++;
        }
    }
}
