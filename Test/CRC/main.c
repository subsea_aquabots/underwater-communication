#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <inttypes.h>

int dataToSend[50];
int ascciForCRC[100];
bool sendArray[50][2];

int dataLength;

void crc(int addr[100], int num)
{
    int i, j;

    int data[50];

    for (i = 0; i < num; i++)
    {
        for (j = 0; j <2; j++)
        {
            data[i] = addr[i*2] << 8 | addr[i*2+j];
        }
    }

    for (i = 0; i < num/2; i++)
    {
        printf("%i\n", data[i]);

        int poly = 0b10010;
        int calc = data[i] << 4;

        int tmp;

        while (1)
        {
            if (poly > calc) break;
            while (poly < calc)
            {
                poly = poly << 1;
                printf("%i, ",poly);
            }
            poly = poly >> 1;

            printf("%i, ",poly);

            tmp = calc ^ poly;

            printf("%i ^ %i = %i\n", calc, poly, tmp);

            calc = calc ^ poly;

            poly = 0b10010;
        }

        int total = (int)data[i] << 4 | calc;
        printf("%i ", total);

        dataToSend[i] = total;

        printf("\n\n");
    }
}

void fillDataArray()
{
    int i;

    for (i = 0; i < 50; i++)
    {
        if (dataToSend[i] < 0) break;
        printf("\n#%i ", i);
        printf("%i  ", dataToSend[i]);

        int j;

        int ascii = dataToSend[i];

        for (j = 0; j < 10; j++)
        {
            int tmp = (ascii & 0b11000000000000000000) >> 18;
            ascii = ascii << 2;

            sendArray[i*10+j][0] = tmp & 0b10;
            sendArray[i*10+j][1] = tmp & 0b01;

            dataLength++;
        }
    }
}

int main()
{
    dataLength = 0;

    int i;
    for (i = 0; i < 50; i++)
    {
        dataToSend[i] = -1;
    }

    char input[100];

    printf("Enter a string: ");
    scanf("%99[^\n]", &input);

    printf("%s\n", input);

    for (i = 0; i < strlen(input); i++)
    {
        ascciForCRC[i] = (int)input[i];
    }

    crc(ascciForCRC, strlen(input));

    fillDataArray();

    printf("\n\n\n\n");

    for (i = 0; i < dataLength; i++)
    {
        printf("%i%i\n", sendArray[i][0], sendArray[i][1]);
    }

    return 0;
}
